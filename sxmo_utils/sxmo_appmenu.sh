#!/usr/bin/env sh
trap gracefulexit INT TERM

# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "$(dirname "$0")/sxmo_common.sh"

gracefulexit() {
	printf "Gracefully exiting %s\n" "$0">&2
	kill -9 0
}

sxmo_type() {
	sxmo_type.sh -s 200 "$@" # dunno why this is necessary but it sucks without
}

wm="$(sxmo_wm.sh)"

programchoicesinit() {
	XPROPOUT="$(sxmo_wm.sh focusedwindow)"
	WMCLASS="${1:-$(printf %s "$XPROPOUT" | grep app: | cut -d" " -f2- | tr '[:upper:]' '[:lower:]')}"
	if [ -z "$XPROPOUT" ]; then
		printf "sxmo_appmenu: detected no active window, no problem, opening system menu\n" >&2
	else
		printf "sxmo_appmenu: opening menu for wmclass %s\n" "$WMCLASS" >&2
	fi

	case "$WMCLASS" in
	applications )
		# Apps menu
		if [ -x "$XDG_CONFIG_HOME/sxmo/hooks/apps" ]; then
			CHOICES="$("$XDG_CONFIG_HOME/sxmo/hooks/apps")"
		else
			CHOICES="
				$(command -v anbox-launch >/dev/null && echo "$icon_and Anbox       ^ 0 ^ anbox")
				$(command -v audacity   >/dev/null && echo "$icon_mic Audacity    ^ 0 ^ audacity")
				$(command -v gnome-calculator   >/dev/null && echo "$icon_clc Calculator    ^ 0 ^ gnome-calculator")
				$(command -v dino       >/dev/null && echo "$icon_msg Dino        ^ 0 ^ GDK_SCALE=1 dino")
				$(command -v dolphin    >/dev/null && echo "$icon_dir Dolphin     ^ 0 ^ dolphin")
				$(command -v emacs      >/dev/null && echo "$icon_edt Emacs       ^ 0 ^ sxmo_terminal.sh emacs")
				$(command -v epiphany   >/dev/null && echo "$icon_glb Epiphany    ^ 0 ^ epiphany")
				$(command -v firefox    >/dev/null && echo "$icon_ffx Firefox     ^ 0 ^ firefox")
				$(command -v foliate    >/dev/null && echo "$icon_bok Foliate     ^ 0 ^ foliate")
				$([ "$wm" = sway ] && command -v foot       >/dev/null && echo "$icon_trm Foot        ^ 0 ^ foot $SHELL")
				$(command -v foxtrotgps >/dev/null && echo "$icon_gps Foxtrotgps  ^ 0 ^ foxtrotgps")
				$(command -v geany      >/dev/null && echo "$icon_eml Geany       ^ 0 ^ geany")
				$(command -v gedit      >/dev/null && echo "$icon_edt Gedit       ^ 0 ^ gedit")
				$(command -v geeqie     >/dev/null && echo "$icon_img Geeqie      ^ 0 ^ geeqie")
				$(command -v giara      >/dev/null && echo "$icon_red Giara       ^ 0 ^ giara")
				$(command -v gnome-chess>/dev/null && echo "$icon_itm Gnome Chess ^ 0 ^ gnome-chess")
				$(command -v gucharmap  >/dev/null && echo "$icon_inf Gucharmap   ^ 0 ^ gucharmap")
				$(command -v hexchat    >/dev/null && echo "$icon_msg Hexchat     ^ 0 ^ hexchat")
				$(command -v kmail      >/dev/null && echo "$icon_eml KMail       ^ 0 ^ kmail")
				$(command -v kontact    >/dev/null && echo "$icon_msg Kontact ^ 0 ^ kontact")
				$(command -v konversation   >/dev/null && echo "$icon_msg Konversation ^ 0 ^ konversation")
				$(command -v kwrite     >/dev/null && echo "$icon_edt Kwrite      ^ 0 ^ kwrite")
				$(command -v lagrange   >/dev/null && echo "$icon_glb Lagrange    ^ 0 ^ lagrange")
				$(command -v lf         >/dev/null && echo "$icon_dir Lf          ^ 0 ^ sxmo_terminal.sh lf")
				$(command -v lollypop   >/dev/null && echo "$icon_mus Lollypop    ^ 0 ^ lollypop")
				$(command -v marble     >/dev/null && echo "$icon_map Marble      ^ 0 ^ marble")
				$(command -v midori     >/dev/null && echo "$icon_glb Midori      ^ 0 ^ midori")
				$(command -v navit      >/dev/null && echo "$icon_gps Navit       ^ 0 ^ navit")
				$(command -v nheko      >/dev/null && echo "$icon_msg Nheko       ^ 0 ^ nheko")
				$(command -v netsurf    >/dev/null && echo "$icon_glb Netsurf     ^ 0 ^ netsurf")
				$(command -v pidgin     >/dev/null && echo "$icon_msg Pidgin      ^ 0 ^ pidgin")
				$(command -v pure-maps  >/dev/null && echo "$icon_map Pure-Maps   ^ 0 ^ pure-maps")
				$(command -v qutebrowser>/dev/null && echo "$icon_glb Qutebrowser ^ 0 ^ qutebrowser")
				$([ "$wm" = dwm ] && command -v st         >/dev/null && echo "$icon_trm St          ^ 0 ^ st -e $SHELL")
				$(command -v surf       >/dev/null && echo "$icon_glb Surf        ^ 0 ^ surf")
				$(command -v syncthing  >/dev/null && echo "$icon_rld Syncthing          ^ 0 ^ syncthing")
				$(command -v telegram-desktop >/dev/null && echo "$icon_tgm Telegram     ^ 0 ^ telegram-desktop")
				$(command -v termite    >/dev/null && echo "$icon_trm Termite     ^ 0 ^ termite -e  $SHELL")
				$(command -v thunderbird >/dev/null && echo "$icon_eml Thunderbird     ^ 0 ^ thunderbird")
				$(command -v totem      >/dev/null && echo "$icon_mvi Totem       ^ 0 ^ totem")
				$(command -v vlc        >/dev/null && echo "$icon_mvi Vlc         ^ 0 ^ vlc")
				$([ "$wm" = dwm ] && command -v xcalc      >/dev/null && echo "$icon_clc Xcalc       ^ 0 ^ xcalc")
			"
		fi
		WINNAME=Apps
		;;
	config )
		# System Control menu
		CHOICES="
			$icon_phn Modem Toggle               ^ 1 ^ sxmo_modemmonitortoggle.sh
			$icon_phn Modem Restart              ^ 1 ^ sxmo_modemmonitortoggle.sh restart
			$icon_inf Modem Info                 ^ 0 ^ sxmo_modeminfo.sh
			$icon_phl Modem Log                  ^ 0 ^ sxmo_modemlog.sh
			$icon_img Config MMS                 ^ 1 ^ sxmo_mmsdconfig.sh
			$icon_cfg Toggle Bar ^ 0 ^ sxmo_wm.sh togglebar
                        $icon_zzz Auto-suspend $(
                                [ -e "$XDG_CACHE_HOME/sxmo/sxmo.nosuspend" ] && printf "%s" "$icon_tof" || printf "%s" "$icon_ton"
                        ) ^ 1 ^ rm "$XDG_CACHE_HOME/sxmo/sxmo.nosuspend" || touch "$XDG_CACHE_HOME/sxmo/sxmo.nosuspend"
                        $icon_zzz Auto-screen-off $(
                                [ -e "$XDG_CACHE_HOME/sxmo/sxmo.noidle" ] && printf "%s" "$icon_tof" || printf "%s" "$icon_ton"
                        ) ^ 1 ^ (rm "$XDG_CACHE_HOME/sxmo/sxmo.noidle" || touch "$XDG_CACHE_HOME/sxmo/sxmo.noidle") && sxmo_hook_unlock.sh
                        $icon_ror Autorotate $(
                                sxmo_daemons.sh running autorotate -q &&
                                printf "%s" "$icon_ton" || printf "%s" "$icon_tof"
                        ) ^ 1 ^ toggle_daemon 'Autorotate' autorotate sxmo_autorotate.sh
                        $icon_lck Proximity Lock $(
                                sxmo_daemons.sh running proximity_lock -q &&
                                printf "%s" "$icon_ton" || printf "%s" "$icon_tof"
                        ) ^ 1 ^ toggle_daemon 'Proximity Lock' proximity_lock sxmo_proximitylock.sh
			$icon_ror Rotate                     ^ 1 ^ sxmo_rotate.sh rotate
			$icon_cfg Edit configuration         ^ 0 ^ sxmo_terminal.sh $EDITOR $XDG_CONFIG_HOME/sxmo/xinit
		"
		WINNAME=Config
		;;
	audioout )
		# Audio Out menu
		CURRENTDEV="$(sxmo_audiocurrentdevice.sh)"
		CHOICES="
			$icon_hdp Headphones $([ "$CURRENTDEV" = "Headphone" ] && echo "$icon_chk") ^ 1 ^ sxmo_audioout.sh Headphones
			$icon_spk Speaker $([ "$CURRENTDEV" = "Line Out" ] && echo "$icon_chk")     ^ 1 ^ sxmo_audioout.sh Speaker
			$icon_phn Earpiece $([ "$CURRENTDEV" = "Earpiece" ] && echo "$icon_chk")    ^ 1 ^ sxmo_audioout.sh Earpiece
			$icon_mut None $([ "$CURRENTDEV" = "None" ] && echo "$icon_chk")            ^ 1 ^ sxmo_audioout.sh None
		"
		WINNAME=Audio
		;;
	power )
		# Power menu
		CHOICES="
			$icon_lck Lock               ^ 0 ^ sxmo_screenlock.sh lock
			$icon_lck Lock (Screen off)  ^ 0 ^ sxmo_screenlock.sh off
			$icon_zzz Suspend            ^ 0 ^ sxmo_screenlock.sh lock && sxmo_screenlock.sh crust
			$icon_out Logout             ^ 0 ^ pkill -9 dwm
			$icon_rld Reboot             ^ 0 ^ reboot
			$icon_pwr Poweroff           ^ 0 ^ poweroff
		"
		WINNAME=Power
		;;
	*foot*|*st* )
		# First we try to handle the app running inside the terminal:
		WMNAME="${1:-$(printf %s "$XPROPOUT" | grep title: | cut -d" " -f2- | tr '[:upper:]' '[:lower:]')}"
		if printf %s "$WMNAME" | grep -qi -E "(vi|vim|vis|nvim|neovim|kakoune)"; then
			#Vim in foot
			CHOICES="
				$icon_aru Scroll up        ^ 1 ^ sxmo_type -M Ctrl u
				$icon_ard Scroll down      ^ 1 ^ sxmo_type -M Ctrl d
				$icon_trm Command prompt   ^ 0 ^ sxmo_type -k Escape -s 300 ':'
				$icon_cls Save             ^ 0 ^ sxmo_type -k Escape -s 300 ':w' -k Return
				$icon_cls Save and Quit    ^ 0 ^ sxmo_type -k Escape -s 300 ':wq' -k Return
				$icon_cls Quit without saving  ^ 0 ^ sxmo_type -k Escape -s 300 ':q!' -k Return
				$icon_pst Paste Selection  ^ 0 ^ sxmo_type -k Escape -s 300 -k quotedbl -k asterisk -k p
				$icon_pst Paste Clipboard  ^ 0 ^ wl-paste
				$icon_fnd Search           ^ 0 ^ sxmo_type -k Escape -s 300 /
				$icon_zmi Zoom in          ^ 1 ^ sxmo_type -k Prior
				$icon_zmo Zoom out         ^ 1 ^ sxmo_type -k Next
				$icon_mnu Terminal menu    ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=Vim
		elif printf %s "$WMNAME" | grep -qi -w "nano"; then
			#Nano in foot
			CHOICES="
				$icon_aru Scroll up       ^ 1 ^ sxmo_type -k Prior
				$icon_ard Scroll down     ^ 1 ^ sxmo_type -k Next
				$icon_sav Save            ^ 0 ^ sxmo_type -M Ctrl o
				$icon_cls Quit            ^ 0 ^ sxmo_type -M Ctrl x
				$icon_pst Paste           ^ 0 ^ sxmo_type -M Ctrl u
				$icon_itm Type complete   ^ 0 ^ sxmo_type -M Shift -M Ctrl u
				$icon_cpy Copy complete   ^ 0 ^ sxmo_type -M Shift -M Ctrl i
				$icon_zmi Zoom in         ^ 1 ^ sxmo_type -k Prior
				$icon_zmo Zoom out        ^ 1 ^ sxmo_type -k Next
				$icon_mnu Terminal menu   ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=Nano
		elif printf %s "$WMNAME" | grep -qi -w "tuir"; then
			#tuir (reddit client) in foot
			CHOICES="
				$icon_aru Previous      ^ 1 ^ sxmo_type k
				$icon_ard Next          ^ 1 ^ sxmo_type j
				$icon_aru Scroll up     ^ 1 ^ sxmo_type -k Prior
				$icon_ard Scroll down   ^ 1 ^ sxmo_type -k Next
				$icon_ret Open          ^ 0 ^ sxmo_type o
				$icon_arl Back          ^ 0 ^ sxmo_type h
				$icon_arr Comments      ^ 0 ^ sxmo_type l
				$icon_edt Post          ^ 0 ^ sxmo_type c
				$icon_rld Refresh       ^ 0 ^ sxmo_type r
				$icon_cls Quit          ^ 0 ^ sxmo_type q
				$icon_zmi Zoom in       ^ 1 ^ sxmo_type -k Prior
				$icon_zmo Zoom out      ^ 1 ^ sxmo_type -k Next
				$icon_mnu Terminal menu ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=tuir
		elif printf %s "$WMNAME" | grep -qi -w "w3m"; then
			#w3m
			CHOICES="
				$icon_arl Back          ^ 1 ^ sxmo_type b
				$icon_glb Goto URL        ^ 1 ^ sxmo_type u
				$icon_arr Next Link       ^ 1 ^ sxmo_type -k Tab
				$icon_arl Previous Link   ^ 1 ^ sxmo_type -M Shift -k Tab
				$icon_tab Open tab        ^ 0 ^ sxmo_type t
				$icon_cls Close tab       ^ 0 ^ sxmo_type -M Ctrl q
				$icon_itm Next tab        ^ 1 ^ sxmo_type -k braceRight
				$icon_itm Previous tab    ^ 1 ^ sxmo_type -k braceLeft
				$icon_zmi Zoom in          ^ 1 ^ sxmo_type -k Prior
				$icon_zmo Zoom out          ^ 1 ^ sxmo_type -k Next
				$icon_mnu Terminal menu   ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=w3m
		elif printf %s "$WMNAME" | grep -qi -w "ncmpcpp"; then
			#ncmpcpp
			CHOICES="
				$icon_lst Playlist        ^ 0 ^ sxmo_type 1
				$icon_fnd Browser         ^ 0 ^ sxmo_type 2
				$icon_fnd Search          ^ 0 ^ sxmo_type 3
				$icon_nxt Next track      ^ 0 ^ sxmo_type -k greater
				$icon_prv Previous track  ^ 0 ^ sxmo_type -k less
				$icon_pau Pause           ^ 0 ^ sxmo_type p
				$icon_stp Stop            ^ 0 ^ sxmo_type s
				$icon_rld Toggle repeat   ^ 0 ^ sxmo_type r
				$icon_sfl Toggle random   ^ 0 ^ sxmo_type z
				$icon_itm Toggle consume  ^ 0 ^ sxmo_type R
				$icon_mnu Terminal menu   ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=ncmpcpp
		elif printf %s "$WMNAME" | grep -qi -w "aerc"; then
			#aerc
			CHOICES="
				$icon_pau Archive	  ^ 1 ^ sxmo_type ':archive flat' -k Return
				$icon_nxt Next Tab	  ^ 0 ^ sxmo_type ':next-tab' -k Return
				$icon_prv Previous Tab	  ^ 0 ^ sxmo_type ':prev-tab' -k Return
				$icon_cls Close Tab	  ^ 0 ^ sxmo_type ':close' -k Return
				$icon_itm Next Part	  ^ 1 ^ sxmo_type ':next-part' -k Return
				$icon_trm xdg-open Part	  ^ 0 ^ sxmo_type ':open' -k Return
			"
			WINNAME=aerc
		elif printf %s "$WMNAME" | grep -qi -E -w "(less|mless)"; then
			#less
			CHOICES="
				$icon_arr Page next       ^ 1 ^ sxmo_type ':n' -k Return
				$icon_arl Page previous   ^ 1 ^ sxmo_type ':p' -k Return
				$icon_cls Quit            ^ 0 ^ sxmo_type q
				$icon_zmi Zoom in         ^ 1 ^ sxmo_type -M Ctrl +
				$icon_zmo Zoom out        ^ 1 ^ sxmo_type -M Ctrl -k Minus
				$icon_aru Scroll up       ^ 1 ^ sxmo_type -k Prior
				$icon_ard Scroll down     ^ 1 ^ sxmo_type -k Next
				$icon_mnu Terminal menu ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=less
		elif printf %s "$WMNAME" | grep -qi -w "weechat"; then
			#weechat
			CHOICES="
				$icon_msg Hotlist Next            ^ 1 ^ sxmo_type -M Alt a
				$icon_arl History Previous        ^ 1 ^ sxmo_type -M Alt -k Less
				$icon_arr History Next            ^ 1 ^ sxmo_type -M Alt -k Greater
				$icon_trm Buffer                  ^ 0 ^ sxmo_type '/buffer '
				$icon_aru Scroll up               ^ 1 ^ sxmo_type -k Prior
				$icon_ard Scroll down             ^ 1 ^ sxmo_type -k Next
				$icon_mnu Terminal menu ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=weechat
		elif printf %s "$WMNAME" | grep -qi -w "sms"; then
			# TODO: Ensure $number is valid.
			number="$(printf %s "$WMNAME" | sed -e 's|^\"||' -e 's|\"$||' | cut -f1 -d' ')"
			#sms
			CHOICES="
				$icon_msg Conversation   ^ 0 ^ sxmo_terminal.sh sxmo_modemtext.sh conversationloop $number
				$icon_msg Reply          ^ 0 ^ sxmo_modemtext.sh sendtextmenu $number
				$icon_phn Call           ^ 0 ^ sxmo_modemdial.sh $number
				$([ -d "$LOGDIR/$number/attachments" ] && echo "$icon_att View Attachments ^ 1 ^ sxmo_files.sh $LOGDIR/$number/attachments")
				$(sxmo_contacts.sh --all | grep -q -e "$number" || echo "$icon_usr Add to contacts ^ 0 ^ sxmo_contactmenu.sh newcontact $number")
				$icon_aru Scroll up       ^ 1 ^ sxmo_type -M Shift -M Ctrl b
				$icon_ard Scroll down     ^ 1 ^ sxmo_type -M Shift -M Ctrl f
				$icon_mnu Terminal menu ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=sms
		elif printf %s "$WMNAME" | grep -qi -w "cmus"; then
			# cmus
			# requires `:set set_term_title=false` in cmus to match the application
			CHOICES="
				$icon_itm Play            ^ 0 ^ cmus-remote -p
				$icon_pau Pause           ^ 0 ^ cmus-remote -u
				$icon_stp Stop            ^ 0 ^ cmus-remote -s
				$icon_nxt Next track      ^ 0 ^ cmus-remote -n
				$icon_prv Previous track  ^ 0 ^ cmus-remote -r
				$icon_rld Toggle repeat   ^ 0 ^ cmus-remote -R
				$icon_sfl Toggle random   ^ 0 ^ cmus-remote -S
				$icon_mnu Terminal menu   ^ 0 ^ sxmo_appmenu.sh $WMCLASS
			"
			WINNAME=cmus
		else
			# Now we fallback to the default terminal menu
			case "$WMCLASS" in
				*st*)
					STSELMODEON="$(
						printf %s "$XPROPOUT" | grep -E '^_ST_SELMODE.+=' | cut -d= -f2 | tr -d ' '
					)"
					CHOICES="
						$icon_itm Type complete   ^ 0 ^ sxmo_type -M Ctrl -M Shift -k u
						$icon_cpy Copy complete   ^ 0 ^ sxmo_type -M Ctrl -M Shift -k i
						$icon_itm Selmode $(
						  [ "$STSELMODEON" = 1 ] &&
						  printf %b 'On → Off' ||
						  printf %b 'Off → On'
						  printf %b '^ 0 ^ sxmo_type -M Ctrl -M Shift -k s'
						)
						$([ "$STSELMODEON" = 1 ] && echo 'Copy selection ^ 0 ^ sxmo_type -M Ctrl -M Shift -k c')
						$icon_pst Paste           ^ 0 ^ sxmo_type -M Ctrl -M Shift -k v
						$icon_zmi Zoom in         ^ 1 ^ sxmo_type -M Ctrl -M Shift -k Prior
						$icon_zmo Zoom out        ^ 1 ^ sxmo_type -M Ctrl -M Shift -k Next
						$icon_aru Scroll up       ^ 1 ^ sxmo_type -M Ctrl -M Shift -k b
						$icon_ard Scroll down     ^ 1 ^ sxmo_type -M Ctrl -M Shift -k f
						$icon_a2x Invert          ^ 1 ^ sxmo_type -M Ctrl -M Shift -k x
						$icon_kbd Hotkeys         ^ 0 ^ sxmo_appmenu.sh sthotkeys
					"
					WINNAME=St
					;;
				*foot*)
					CHOICES="
						$icon_cpy Copy		  ^ 0 ^ sxmo_type -M Shift -M Ctrl c
						$icon_pst Paste           ^ 0 ^ sxmo_type -M Shift -M Ctrl v
						$icon_zmi Zoom in         ^ 1 ^ sxmo_type -M Ctrl +
						$icon_zmo Zoom out        ^ 1 ^ sxmo_type -M Ctrl -k Minus
						$icon_aru Scroll up       ^ 1 ^ sxmo_type -M Shift -k Prior
						$icon_ard Scroll down     ^ 1 ^ sxmo_type -M Shift -k Next
						$icon_lnk URL Mode        ^ 0 ^ sxmo_type -M Shift -M Ctrl -k u
						$icon_kbd Hotkeys         ^ 0 ^ sxmo_appmenu.sh sthotkeys
					"
					WINNAME=Foot
					;;
			esac
		fi
	;;
	*surf* )
		# Surf
		CHOICES="
			$icon_glb Navigate    ^ 0 ^ sxmo_type -M Ctrl g
			$icon_lnk Link Menu   ^ 0 ^ sxmo_type -M Ctrl d
			$icon_flt Pipe URL    ^ 0 ^ sxmo_urlhandler.sh
			$icon_fnd Search Page ^ 0 ^ sxmo_type -M Ctrl f
			$icon_fnd Find Next   ^ 0 ^ sxmo_type -M Ctrl n
			$icon_zmi Zoom      ^ 1 ^ sxmo_type -M Shift -M Ctrl k
			$icon_zmo Zoom      ^ 1 ^ sxmo_type -M Shift -M Ctrl j
			$icon_aru Scroll    ^ 1 ^ sxmo_type -M Shift -k Space
			$icon_ard Scroll    ^ 1 ^ sxmo_type -k Space
			$icon_itm JS Toggle   ^ 1 ^ sxmo_type -M Shift -M Ctrl s
			$icon_arl History   ^ 1 ^ sxmo_type -M Ctrl h
			$icon_arr History   ^ 1 ^ sxmo_type -M Ctrl l
			$icon_rld Refresh     ^ 0 ^ sxmo_type -M Shift -M Ctrl r
		"
		WINNAME=Surf
		;;
	*firefox* )
		# Firefox
		CHOICES="
			$icon_flt Pipe URL          ^ 0 ^ sxmo_urlhandler.sh
			$icon_tab New Tab           ^ 0 ^ sxmo_type -M Ctrl t
			$icon_win New Window        ^ 0 ^ sxmo_type -M Ctrl n
			$icon_cls Close Tab         ^ 0 ^ sxmo_type -M Ctrl w
			$icon_zmi Zoom            ^ 1 ^ sxmo_type -M Ctrl -k plus
			$icon_zmo Zoom            ^ 1 ^ sxmo_type -M Ctrl -k minus
			$icon_arl History        ^ 1 ^ sxmo_type -M Alt -k Left
			$icon_arr History        ^ 1 ^ sxmo_type -M Alt -k Right
			$icon_rld Refresh     ^ 0 ^ sxmo_type -M Shift -M Ctrl r
		"
		WINNAME=Firefox
		;;
	*foxtrot* )
		# Foxtrot GPS
		CHOICES="
			$icon_itm Locations           ^ 0 ^ sxmo_gpsutil.sh menulocations
			$icon_cpy Copy                ^ 1 ^ sxmo_gpsutil.sh copy
			$icon_pst Paste               ^ 0 ^ sxmo_gpsutil.sh paste
			$icon_itm Drop Pin            ^ 0 ^ sxmo_gpsutil.sh droppin
			$icon_fnd Region Search       ^ 0 ^ sxmo_gpsutil.sh menuregionsearch
			$icon_itm Region Details      ^ 0 ^ sxmo_gpsutil.sh details
			$icon_zmi Zoom              ^ 1 ^ sxmo_type i
			$icon_zmo Zoom              ^ 1 ^ sxmo_type o
			$icon_itm Map Type            ^ 0 ^ sxmo_gpsutil.sh menumaptype
			$icon_itm Panel Toggle        ^ 1 ^ sxmo_type m
			$icon_itm GPSD Toggle         ^ 1 ^ sxmo_type a
			$icon_usr Locate Me           ^ 0 ^ sxmo_gpsutil.sh gpsgeoclueset
		"
		WINNAME=Maps
		;;
	audiolvl )
		CHOICES="
			$icon_mus 0                   ^ 1 ^ pactl -- set-sink-volume 0 0%
			$icon_mus 5                   ^ 1 ^ pactl -- set-sink-volume 0 5%
                        $icon_mus 10                  ^ 1 ^ pactl -- set-sink-volume 0 10%
                        $icon_mus 15                  ^ 1 ^ pactl -- set-sink-volume 0 15%
                        $icon_mus 20                  ^ 1 ^ pactl -- set-sink-volume 0 20%
                        $icon_mus 25                  ^ 1 ^ pactl -- set-sink-volume 0 25%
                        $icon_mus 30                  ^ 1 ^ pactl -- set-sink-volume 0 30%
                        $icon_mus 35                  ^ 1 ^ pactl -- set-sink-volume 0 35%
                        $icon_mus 40                  ^ 1 ^ pactl -- set-sink-volume 0 40%
                        $icon_mus 45                  ^ 1 ^ pactl -- set-sink-volume 0 45%
                        $icon_mus 50                  ^ 1 ^ pactl -- set-sink-volume 0 50%
                        $icon_mus 55                  ^ 1 ^ pactl -- set-sink-volume 0 55%
                        $icon_mus 60                  ^ 1 ^ pactl -- set-sink-volume 0 60%
                        $icon_mus 65                  ^ 1 ^ pactl -- set-sink-volume 0 65%
                        $icon_mus 70                  ^ 1 ^ pactl -- set-sink-volume 0 70%
                        $icon_mus 75                  ^ 1 ^ pactl -- set-sink-volume 0 75%
                        $icon_mus 80                  ^ 1 ^ pactl -- set-sink-volume 0 80%
                        $icon_mus 85                  ^ 1 ^ pactl -- set-sink-volume 0 85%
                        $icon_mus 90                  ^ 1 ^ pactl -- set-sink-volume 0 90%
                        $icon_mus 95                  ^ 1 ^ pactl -- set-sink-volume 0 95%
                        $icon_mus 100                 ^ 1 ^ pactl -- set-sink-volume 0 100%
		"
		WINNAME=Audio
		;;
	brightnesslvl )
		CHOICES="
			$icon_aru 1                   ^ 0 ^ sxmo_brightness.sh setvalue 1
			$icon_aru 5                   ^ 0 ^ sxmo_brightness.sh setvalue 5
                        $icon_aru 10                  ^ 0 ^ sxmo_brightness.sh setvalue 10
                        $icon_aru 15                  ^ 0 ^ sxmo_brightness.sh setvalue 15
                        $icon_aru 20                  ^ 0 ^ sxmo_brightness.sh setvalue 20
                        $icon_aru 25                  ^ 0 ^ sxmo_brightness.sh setvalue 25
                        $icon_aru 30                  ^ 0 ^ sxmo_brightness.sh setvalue 30
                        $icon_aru 35                  ^ 0 ^ sxmo_brightness.sh setvalue 35
                        $icon_aru 40                  ^ 0 ^ sxmo_brightness.sh setvalue 40
                        $icon_aru 45                  ^ 0 ^ sxmo_brightness.sh setvalue 45
                        $icon_aru 50                  ^ 0 ^ sxmo_brightness.sh setvalue 50
                        $icon_aru 55                  ^ 0 ^ sxmo_brightness.sh setvalue 55
                        $icon_aru 60                  ^ 0 ^ sxmo_brightness.sh setvalue 60
                        $icon_aru 65                  ^ 0 ^ sxmo_brightness.sh setvalue 65
                        $icon_aru 70                  ^ 0 ^ sxmo_brightness.sh setvalue 70
                        $icon_aru 75                  ^ 0 ^ sxmo_brightness.sh setvalue 75
                        $icon_aru 80                  ^ 0 ^ sxmo_brightness.sh setvalue 80
                        $icon_aru 85                  ^ 0 ^ sxmo_brightness.sh setvalue 85
                        $icon_aru 90                  ^ 0 ^ sxmo_brightness.sh setvalue 90
                        $icon_aru 95                  ^ 0 ^ sxmo_brightness.sh setvalue 95
                        $icon_aru 100                 ^ 0 ^ sxmo_brightness.sh setvalue 100
		"
		WINNAME=Brightness
		;;			
	* )
		# Default system menu (no matches)
		CHOICES="
			$icon_grd App Search					     ^ 0 ^ sxmo_keyboard.sh open; dmenu_run -c -fn Terminus-14 -l 20
			$icon_grd Apps                                               ^ 0 ^ sxmo_appmenu.sh applications
			$icon_phn Dialer                                             ^ 0 ^ sxmo_modemdial.sh
			$icon_msg Texts                                              ^ 0 ^ sxmo_modemtext.sh
			$icon_usr Contacts                                           ^ 0 ^ sxmo_contactmenu.sh
			$icon_bth Bluetooth 										 ^ 1 ^ sxmo_bluetoothmenu.sh
			$(command -v megapixels >/dev/null && echo "$icon_cam Camera ^ 0 ^ GDK_SCALE=2 megapixels")
			$icon_fll Flashlight $(
				grep -qE '^0$' /sys/class/leds/white:flash/brightness &&
				printf %b "Off → On" ||  printf %b "On → Off";
				printf %b "^ 1 ^ sxmo_flashtoggle.sh"
			)
			$icon_net Networks                                           ^ 0 ^ sxmo_networks.sh
			$icon_mus Audio Out                                          ^ 0 ^ sxmo_appmenu.sh audioout
			$icon_mus Audio Level					     ^ 0 ^ sxmo_appmenu.sh audiolvl
			$icon_aru Brightness Level				     ^ 0 ^ sxmo_appmenu.sh brightnesslvl
			$icon_cfg Config                                             ^ 0 ^ sxmo_appmenu.sh config
			$icon_pwr Power                                              ^ 0 ^ sxmo_appmenu.sh power
		"
		WINNAME=Sys
		;;
	esac
}

getprogchoices() {
	# E.g. sets CHOICES var
	programchoicesinit "$@"


	# For the Sys menu decorate at top with notifications if >1 notification
	if [ "$WINNAME" = "Sys" ]; then
		NNOTIFICATIONS="$(find "$NOTIFDIR" -type f | wc -l)"
		if [ "$NNOTIFICATIONS" -gt 0 ]; then
			CHOICES="
				$icon_bel Notifications ($NNOTIFICATIONS) ^ 0 ^ sxmo_notificationsmenu.sh
				$CHOICES
			"
		fi
	fi

	#shellcheck disable=SC2044
	for NOTIFFILE in $(find "$NOTIFDIR" -name 'incomingcall*_notification'); do
		NOTIFACTION="$(head -n1 "$NOTIFFILE")"
		MESSAGE="$(tail -1 "$NOTIFFILE")"
		CHOICES="
			$icon_phn $MESSAGE ^ 0 ^ $NOTIFACTION
			$CHOICES
		"
		break
	done

	# Decorate menu at bottom w/ system menu entry if not system menu
	echo $WINNAME | grep -qv Sys && CHOICES="
		$CHOICES
		$icon_mnu System Menu   ^ 0 ^ sxmo_appmenu.sh sys
	"

	# Decorate menu at bottom w/ close menu entry
	CHOICES="
		$CHOICES
		$icon_cls Close Menu    ^ 0 ^ quit
	"

	CHOICES="$(printf "%s\n" "$CHOICES" | xargs -0 echo | sed '/^[[:space:]]*$/d' | awk '{$1=$1};1')"
}

quit() {
	exit 0
}

mainloop() {
	getprogchoices "$@"
	PICKED="$(
		printf "%s\n" "$CHOICES" |
		cut -d'^' -f1 |
		sxmo_dmenu.sh -i -p "$WINNAME"
	)" || quit
	LOOP="$(printf "%s\n" "$CHOICES" | grep -m1 -F "$PICKED" | cut -d '^' -f2)"
	CMD="$(printf "%s\n" "$CHOICES" | grep -m1 -F "$PICKED" | cut -d '^' -f3)"

	printf "%s\n" "sxmo_appmenu: Eval: <$CMD> from picked <$PICKED> with loop <$LOOP>">&2

	if printf %s "$LOOP" | grep -q 1; then
		eval "$CMD"
		mainloop "$@"
	else
		eval "$CMD" &
		wait
		quit
	fi
}

mainloop "$@"
