ACTION="$1"
STATE="$(sxmo_screenlock.sh getCurState)"

if [[ "$STATE" == "crust" || "$STATE" == "off" || "$STATE" == "lock" ]]; then
	if [[ "$ACTION" != "volup_three" && "$ACTION" != "voldown_three" && "$ACTION" != "powerbutton_three" && "$ACTION" != "powerbutton_two" ]]; then
	        exit 0 #we're locked, don't process the rest of the script
	fi
fi

case "$ACTION" in
	"volup_one")
		sxmo_appmenu.sh &
		exit 0
		;;
	"volup_two")
		sxmo_appmenu.sh audiolvl
		exit 0
		;;
	"volup_three")
		sxmo_flashtoggle.sh
		exit 0
		;;
        "voldown_one")
		sxmo_rotate.sh &
                exit 0
                ;;
	"voldown_two")
		sxmo_appmenu.sh brightnesslvl
		exit 0
		;;
	"voldown_three")
		sxmo_killwindow.sh
		exit 0
		;;
        "powerbutton_one")
		sxmo_keyboard.sh toggle
		exit 0
                ;;
        "powerbutton_two")
		if [[ "$STATE" == "crust" || "$STATE" == "off" || "$STATE" == "lock" ]]; then
			sxmo_screenlock.sh unlock
		else
	                sxmo_screenlock.sh off
		fi
		exit 0
                ;;
        "powerbutton_three")
                if [[ "$STATE" == "crust" || "$STATE" == "off" || "$STATE" == "lock" ]]; then
                        sxmo_screenlock.sh unlock
                else
			sxmo_screenlock.sh crust
		fi
                exit 0
                ;;
esac
#sxmo_workspace.sh move-previous
#sxmo_workspace.sh move-next

