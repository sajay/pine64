xrandr --newmode $1-$2-$3 $(cvt $1 $2 $3 | tail -1 | cut -d' ' -f3-)
xrandr --addmode DSI-1 $1-$2-$3
xrandr --output DSI-1 --mode $1-$2-$3
