sxmo_screenlock.sh updateLed
#Show state of Modem Service
source /bin/sxmo_common.sh

if [[ "$(systemctl show ModemManager --property=SubState | cut -d'=' -f2)" == "dead" ]]; then
        printf " ""$icon_phn"" X"
elif [[ "$(systemctl show ModemManager --property=SubState | cut -d'=' -f2)" == "running" ]]; then
        printf " ""$icon_phn"" $icon_chk"
else
        printf " ""$icon_phn"" ^`^i ^`^i?"
fi

#Show state of Newwork Service
if [[ "$(systemctl show NetworkManager --property=SubState | cut -d'=' -f2)" == "dead" ]]; then
        printf " ""$icon_net""  X"
elif [[ "$(systemctl show NetworkManager --property=SubState | cut -d'=' -f2)" == "running" ]]; then
        printf " ""$icon_net""  $icon_chk"
else
        printf " ""$icon_net""  ?"
fi

#Show state of Bluetooth Service
if [[ "$(systemctl show bluetooth --property=SubState | cut -d'=' -f2)" == "dead" ]]; then
        printf " ""$icon_bth""  X"
elif [[ "$(systemctl show bluetooth --property=SubState | cut -d'=' -f2)" == "running" ]]; then
        printf " ""$icon_bth""  $icon_chk"
else
        printf " ""$icon_bth""  ?"
fi
